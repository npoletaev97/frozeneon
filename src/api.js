export async function get(url) {
    let response = await fetch(url)

    if (response.status === 200) {
        let jsonResponse = await response.json();
        return jsonResponse
    }

    throw new Error(response.status.toString());
}