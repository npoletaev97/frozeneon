import Vue from 'vue'
import Vuex from 'vuex'
import { get } from '../api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    searchResults: null,
    packageFiles: null,
    packageUsageStats: null
  },
  getters: {
    searchResults: (state) => state.searchResults,
    packageFiles: (state) => state.packageFiles,
    totalPackageFiles: (state) => state.packageFiles?.files.length,
    totalPackageSize: (state) => state.packageFiles?.files.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.size
    }, 0),
    packageUsageStats: (state) => state.packageUsageStats,
  },
  mutations: {
    setSearchResults(state, searchResults) {
      state.searchResults = searchResults
    },
    setPackageFiles(state, packageFiles) {
      state.packageFiles = packageFiles
    },
    setPackageUsageStats(state, packageUsageStats) {
      state.packageUsageStats = packageUsageStats
    }
  },
  actions: {
    async searchPackage({ commit }, packageName) {
      try {
        let jsonResponse = await get(`https://registry.npmjs.com/-/v1/search?text=${packageName}&size=20`)
        commit('setSearchResults', jsonResponse.objects)
      } catch (error) {
        return error
      }
    },

    async getPackageFiles({ commit }, npmPackage) {
      try {
        let jsonResponse = await get(`https://data.jsdelivr.com/v1/package/npm/${npmPackage.name}@${npmPackage.version}/flat`)
        commit('setPackageFiles', jsonResponse)
      } catch (error) {
        return error
      }
    },

    async getPackageUsageStats({ commit }, npmPackage) {
      try {
        let jsonResponse = await get(`https://data.jsdelivr.com/v1/package/npm/${npmPackage.name}/stats/week`)
        commit('setPackageUsageStats', jsonResponse)
      } catch (error) {
        return error
      }
    }
  }
})